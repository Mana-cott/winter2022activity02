import java.util.Scanner;
public class PartThree{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		System.out.println("Please insert the length of one of the sides of your square!");
		double squareSide = scan.nextDouble();
		System.out.println("Your square squared is: " + AreaComputations.areaSquare(squareSide));
		System.out.println("Please insert the length of your rectangle!");
		double recLength = scan.nextDouble();
		System.out.println("Please insert the width of your rectangle!");
		double recWidth = scan.nextDouble();
		AreaComputations ac = new AreaComputations();
		double rectangleArea = ac.areaRectangle(recLength, recWidth);
		System.out.println("Your rectangle's area is: " + rectangleArea);
	}
}